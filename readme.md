## Requirements:
* i3-gaps
* polybar
* compton
* feh
* i3lock-fancy
* rofi
* unclutter
* copy fonts from polybar/fonts to /usr/share/fonts
